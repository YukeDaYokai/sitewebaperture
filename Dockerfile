FROM nginx:latest
# Dans la commande suivante, le . signifie que l'on prend tous les documents dans le
# dossiers où l'on se situe, et le chemin qui suit correspond au dossier créé dans le 
# container (point d'arrivée des données)
COPY . /usr/share/nginx/html/