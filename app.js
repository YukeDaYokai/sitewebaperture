/*

LE CODE SUIVANT CORRESPOND A UN EXEMPLE DONNE PAR LE FORMATEUR (NICOLAS) :

const articleElements = document.querySelectorAll('.page article');

for(let i=0;i<articleElements.length;i++){
    console.log(articleElements[i])
}

// Pour ajouter des puces :

let htmlToInsert='';

articleElements.forEach(function(article){
    htmlToInsert += `<div class ="toto"><span>Bonjour</span></div>`
    // Le symbole ` se nomme la backtick
})
console.log(htmlToInsert)
pucesContainer.innerHTML=htmlToInsert;

// Pour ajouter un event Listener sur quelque chose de spécifique, il faut le sélectionner :
const buttonElement = document.getElementById('coucoubutton');

buttonElement.addEventListener('click',function(event){
    console.log(event);
})

// Si on veut déclencher une action à chaque clic sur le body, mais plus précisement sur l'aside :
document.body.addEventListener('click',function(e){
    if(e.target.closest('aside')){
        console.log('click on aside !')
    }
})

*/


/*

Plusieurs étapes à réaliser :
- Récupérer des photos et les ajouter à l'HTML
- Créer une fonction en JS qui permet d'appeler un/des élément(s) :
    - Un tableau pour savoir quel élément est affiché (son index)
    - Une boucle (3 + 1 => 0, si 3 éléments)
    - Stocker la taille du tableau :
        - .length
        - .forEach()
- Créer un slider

*/

// LE CODE SUIVANT CORRESPOND A CELUI QUI SERA UTILISE DANS NOTRE PROJET :

// On crée une constante pour récupérer nos images dans un tableau
// Pour réaliser le querySelector, indiquer une des classes du parent, et ensuite l'objet à récupérer : ici les images
// Ne pas oublier d'inscrire un script dans le fichier html

var myIndex = 0;
carousel();

function carousel() {
    const imgSlider = document.querySelectorAll("div.hero__img img");
    
    // On boucle nos images :
    for (let i = 0; i < imgSlider.length; i++) {
        imgSlider[i].style.display = "none";
    } myIndex++;
    if (myIndex > imgSlider.length) { myIndex = 1 }
    imgSlider[myIndex - 1].style.display = "block";

    // En écrivant la ligne suivante, les images s'alterne toutes les 2 secondes en suivant la boucle.
    // setTimeout(carousel, 3000);
}

// ---------------------------------------------------------------


// CREATION DE L'EVENT LISTENER POUR LE BOUTON DE CHANGEMENT D'IMAGE GAUCHE
const buttonElement = document.getElementById("buttonLeft");

buttonElement.addEventListener('click',function(event){
    const imgSlider = document.querySelectorAll("div.hero__img img");

    for (let i = 0; i < imgSlider.length; i++) {
        imgSlider[i].style.display = "none";
    }
    if (myIndex===imgSlider.length-1){
        myIndex=0
    }else{myIndex++}
    // if (myIndex > imgSlider.length) { myIndex = 1 }
    imgSlider[myIndex].style.display = "block";
})

// Si on veut déclencher une action à chaque clic sur le body, mais plus précisement sur l'aside :
buttonElement.addEventListener('click',function(event){
    if(event.target===buttonElement){
        console.log('click on button !')
    }
})


// CREATION DE L'EVENT LISTENER POUR LE BOUTON DE CHANGEMENT D'IMAGE DROITE
const buttonElementRight = document.getElementById("buttonRight");

buttonElementRight.addEventListener('click',function(event){
    const imgSlider = document.querySelectorAll("div.hero__img img");

    for (let i = 0; i < imgSlider.length; i++) {
        imgSlider[i].style.display = "none";
    }
    if (myIndex===0){myIndex=imgSlider.length-1}
    else{myIndex--}
    // JE NE COMPRENDS PAS LA LIGNE DU DESSUS, MAIS CA FONCTIONNE (ELLE CORRIGE LE FAIT QUE LA BOUCLE DU BOUTON DROIT NE MENE PLUS VERS DU VIDE INFINI)
    // if (myIndex > imgSlider.length) { myIndex = 1 }
    imgSlider[myIndex].style.display = "block";
})

// Si on veut déclencher une action à chaque clic sur le body, mais plus précisement sur l'aside :
buttonElementRight.addEventListener('click',function(event){
    if(event.target===buttonElement){
        console.log('click on button !')
    }
})